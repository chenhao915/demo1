using System;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Windows.Forms;
using Microsoft.Office.Tools.Excel;
using System.Linq;

namespace Demo1
{
    public partial class Ribbon1
    {
        private void Ribbon1_Load(object sender, RibbonUIEventArgs e)
        {

        }
        private void button1_Click(object sender, RibbonControlEventArgs e)
        {
            StringBuilder sb = isSheetWanted("D:\\processed");//euses  processed
        }
        private static StringBuilder isSheetWanted(string v_strDir)
        {
            StringBuilder sb = new StringBuilder();
            Excel.Application app = Globals.ThisAddIn.Application;
            DirectoryInfo dir = new DirectoryInfo(v_strDir);

            FileStream fs = new FileStream("C:\\Users\\陈皓\\Desktop\\log.doc", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter sw = new StreamWriter(fs);

            foreach (FileInfo f in dir.GetFiles("*.xls", SearchOption.AllDirectories))
            {
                bool flag1 = false;//flag1用于判断该工作簿中是否含有$，如果没有则将其删除
                Excel.Workbook book = null;
                try
                {
                    book = app.Workbooks.Open(f.FullName);
                }
                catch (Exception ex1)
                {
                    //ex.Message;//可以用messagebox显示出被抛弃的exception
                    continue;
                }
                //MessageBox.Show(book.Name);
                foreach (Excel.Worksheet worksheet in book.Worksheets)
                {
                    bool flag2 = false;//flag2用于判断该工作表中是否含有$，如果没有则将其删除
                    Excel.Range usedRange = getUsedRange(worksheet);
                    for (int i = 1; i <= usedRange.Rows.Count; i++)
                    {
                        for (int j = 1; j <= usedRange.Columns.Count; j++)
                        {
                            String value = worksheet.Cells[i, j].Value + "";
                            if (value.Contains("$"))
                            {
                                flag1 = true;
                                if (flag2 == false)
                                    sb.Append(book.Path + " " + book.Name + " " + worksheet.Name + "\n");
                                flag2 = true;
                                try
                                {
                                    worksheet.Cells[i, j].Interior.ColorIndex = 3;
                                }
                                catch(Exception ex2)
                                {

                                }
                            }
                        }
                    }
                    if(flag2 == false)
                    {
                        try
                        {
                            worksheet.Delete();
                        }//其实不用考虑只有一个excel无法删除的情况。
                        //只剩一个含有$的--不用删除；
                        //只剩一个不含$的--会在接下来的步骤中整个工作簿被删除
                        catch (Exception ex3)
                        {

                        }
                    }
                }
                try
                {
                    app.Application.DisplayAlerts = false;
                    book.Save();
                }
                catch (Exception ex4)
                {
                    
                }
                book.Close();
                if(flag1 == false)
                {
                    f.Delete();
                }
            }
            sw.WriteLine(@"找到的含有“$”的工作表有：" + "\n" + sb);
            sw.Flush();
            sw.Close();
            app.Quit();
            return sb;
        }
        private static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;//按照26进制来理解
            }
            return columnString;
        }
        private static Excel.Range getUsedRange(Excel.Worksheet worksheet)
        {
            Excel.Range usedRange = worksheet.UsedRange;//初始化
            String[] address_split = usedRange.Address.Split(':');
            if(address_split.Count() != 2)
            {
                return usedRange;
            }
            int maxRow, maxCol;//row行，col列
            getEndRow_Col(usedRange, out maxRow, out maxCol);
            //因为一个函数只能从return得到一个返回值，所以使用out从函数中得到多个返回值
            String col = ExcelColumnFromNumber(maxCol);
            //这个函数的作用是把第几列的数字转化为对应的字母赋值给col
            usedRange = usedRange.get_Range("A1", col + maxRow);//col + maxRow的效果比如：E+3得到E3
            return usedRange;
        }
        private static void getEndRow_Col(Excel.Range usedRange, out int maxRow, out int maxCol)
        {
            int row, col;
            int rowNum = usedRange.Rows.Count;//初始化
            int colNum = usedRange.Columns.Count;//初始化
            Excel.Range cell = null;//初始化
            maxCol = 1;
            maxRow = 1;
            for(row = 1; row <= rowNum; row++)
            {
                bool empty_row_flag = false;
                for (col = 1; col <= colNum; col++)
                {
                    cell = usedRange.Cells[row, col];
                    if(Cell.getCellType(cell) != CELLTYPE.Empty)
                    {
                        empty_row_flag = true;
                        if(maxCol < col)
                        {
                            maxCol = col;
                        }
                    }
                    if(col - 50 >= maxCol)
                    {
                        break;
                    }
                }
                if(empty_row_flag)
                {
                    maxRow = row;
                }else
                {
                    if(row - 50 >= maxRow)
                    {
                        break;
                    }
                }
            }
        }
    }
}