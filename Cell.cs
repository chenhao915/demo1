using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace Demo1
{
    public enum CELLTYPE
    {
        Number,
        Formula,
        String,
        Empty,
        SerialNumber,
        Date,
        None,
    }
    class Cell
    {
        public static CELLTYPE getCellType(Excel.Range cell)
        {
            String formula = cell.FormulaR1C1 + "";
            int num = 0;
            foreach (char c in formula)
            {
                if (c >= '0' && c <= '9')
                {
                    num++;
                }
            }
            if ((num == formula.Length) && (num > 7))
            {
                return CELLTYPE.String;
            }
            String value = cell.Value + "";
            String text = cell.Text;
            if (formula.Equals("NA") || formula.Equals("na") || formula.Equals("N/A") || formula.Equals("n.a."))
            {
                return CELLTYPE.Number;
            }
            if (value.Equals("") || value.Equals("-"))
            {
                return CELLTYPE.Empty;
            }
            if (value.Contains("/"))
            {
                return CELLTYPE.String;
            }
            bool empty_flag = true;
            foreach (char v in value)
            {
                if (!v.ToString().Equals(" "))
                {
                    empty_flag = false;
                    break;
                }
            }
            if (empty_flag)
            {
                return CELLTYPE.Empty;
            }
            int count = 0;
            if (formula.Length >= 1)
            {
                if (formula[0] == '=')
                {
                    if (formula.Contains("R") && formula.Contains("C"))
                    {
                        return CELLTYPE.Formula;
                    }
                    else
                    {
                        bool flag = false;
                        try
                        {
                            String v = (new System.Data.DataTable().Compute(formula.Substring(1), "")).ToString();
                        }
                        catch (Exception ex)
                        {
                            flag = true;
                        }
                        if (flag)
                        {
                            return CELLTYPE.String;
                        }
                        else
                        {
                            return CELLTYPE.Number;
                        }
                    }
                }
                else
                {
                    if (formula[0] == '-' || (formula[0] >= '0' && formula[0] <= '9'))
                    {
                        for (int i = 1; i < formula.Length; i++)
                        {
                            if (formula[i] == '.')
                            {
                                count++;
                            }
                            if (!((formula[i] >= '0' && formula[i] <= '9') || formula[i] == '.'))
                            {
                                return CELLTYPE.String;
                            }
                        }
                        if (count > 1 || text.Contains("/") || value.Contains("/"))
                        {
                            return CELLTYPE.String;
                        }
                        else
                        {
                            return CELLTYPE.Number;
                        }
                    }
                    else
                    {
                        return CELLTYPE.String;
                    }
                }
            }
            return CELLTYPE.Empty;
        }
    }
}
